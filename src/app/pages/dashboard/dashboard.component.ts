import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGrigPlugin from '@fullcalendar/timegrid';
import {FullCalendarComponent} from '@fullcalendar/angular';
import {EventInput} from '@fullcalendar/core/structs/event';
import {BarberService} from '../../service/barber.service';
import {HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
    @ViewChild('calendarCustom') calendar: FullCalendarComponent;

    public calendarPlugins = [resourceTimeGridPlugin, dayGridPlugin, timeGrigPlugin, interactionPlugin];
    public resources = [];
    public events: EventInput[];

    constructor(private changeDetectorRef: ChangeDetectorRef, private barberService: BarberService) {
        this.events = [];
    }

    ngOnInit(): void {
        this.resources = [
            {id: 'a', title: 'NIO'},
            {id: 'b', title: 'BENASSAI'},
            {id: 'c', title: 'LUOOMI'},
            {id: 'd', title: 'BINO'},
            {id: 'e', title: 'MK8S'},
            {id: 'f', title: 'MELOCCARI'},
        ];

        this.barberService.isLogged().subscribe(isLogged => {
            if (!isLogged) {
                window.location.href = `${environment.api_url}login/google`;
            }
        });
    }


    handleDateClick(argument) {
        this.events = this.events.concat({
            title: 'ICHHEHALEAO',
            start: argument.date,
            resourceId: argument.resource.id
        });
        this.changeDetectorRef.detectChanges();
    }

    ngAfterViewInit(): void {

    }
}
