import {Injectable} from '@angular/core';
import {HttpService} from './http.service';

@Injectable({
    providedIn: 'root'
})
export class BarberService {

    constructor(private httpService: HttpService) {
    }

    public getEvents(filter): any {
        return this.httpService.get('events', filter);
    }

    public isLogged(): any {
        return this.httpService.get('isLogged');
    }

    public googleLogin(): any {
        return this.httpService.get('login/google');
    }
}
