import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient) {
    }

    private getHttpHeaders(): HttpHeaders {

        let headers = new HttpHeaders();
        headers = headers.append('Accept', 'application/json');
        headers = headers.append('Content-type', 'application/json');
        return headers;
    }

    public get(path: string, param?: any) {
        return this.http.get(`${environment.api_url}${path}`, {
            params: param,
            headers: this.getHttpHeaders()
        });
    }

    public post(path: string, body?: any) {
        return this.http.post(`${environment.api_url}${path}`, body, {
            headers: this.getHttpHeaders()
        });
    }

    public put(path: string, body?: any) {
        return this.http.put(`${environment.api_url}${path}`, body, {
            headers: this.getHttpHeaders()
        });
    }
}
