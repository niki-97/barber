import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

    constructor(private router: Router) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const token = localStorage.getItem('token');
        if (token) {
            req = req.clone({
                setHeaders: {
                    Accept: 'application/json',
                    Authorization: token,
                },
            });
        } else {
            this.redirectToLogin();
        }

        return next.handle(req).pipe(
            tap((event) => {
                    if (event instanceof HttpResponse) {
                        return req;
                    }

                }, (error) => {
                    this.handleError(error);
                }
            )
        );
    }

    private handleError(error) {
        if (error instanceof HttpErrorResponse) {
            const status = error.status;
            switch (status) {
                case 0:
                    // Cors
                    break;

                case 401:
                    // Unauthorized
                    this.redirectToLogin();
                    break;

                case 403:
                    this.redirectToLogin();
                    break;

                case 404:
                    // Not Found
                    break;
            }
        }
        return error;
    }

    private redirectToLogin() {
      console.log('redirect to login');
    }
}

